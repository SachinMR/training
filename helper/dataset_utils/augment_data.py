"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
# -*- coding: utf-8 -*-
import numpy as np
import cv2



def create_random_grayscale(p_img, alpha=(0.0, 1.0)):
    alpha = alpha[0] + np.random.uniform() * (alpha[1] - alpha[0])

    img_gray = cv2.cvtColor(p_img, cv2.COLOR_RGB2GRAY)
    img_gray = np.expand_dims(img_gray, axis=-1)
    img_gray = np.tile(img_gray, (1, 1, 3))

    img_gray = img_gray.astype(np.float32)
    img = p_img.astype(np.float32)
    img = img + alpha * (img_gray - img)
    img = np.clip(img, 0., 255.)
    p_img = img.astype(np.uint8)

    return p_img


def create_random_distort(p_img, hue=18, saturation=1.5, exposure=1.5):
    # determine scale factors
    dhue = np.random.uniform(-hue, hue)
    dsat = np.random.uniform(1. / saturation, saturation)
    dexp = np.random.uniform(1. / exposure, exposure)

    # convert RGB space to HSV space
    p_img = cv2.cvtColor(p_img, cv2.COLOR_RGB2HSV).astype('float')

    # change satuation and exposure
    p_img[:, :, 1] *= dsat
    p_img[:, :, 2] *= dexp

    # change hue
    p_img[:, :, 0] += dhue

    p_img[:, :, 0] = np.clip(p_img[:, :, 0], 0., 179.)
    p_img[:, :, 1] = np.clip(p_img[:, :, 1], 0., 255.)
    p_img[:, :, 2] = np.clip(p_img[:, :, 2], 0., 255.)

    # convert back to RGB from HSV
    return cv2.cvtColor(p_img.astype('uint8'), cv2.COLOR_HSV2RGB)


def create_random_rotate(p_img, p_bounding_boxes, angle=7.):
    angle = np.random.uniform(-angle, angle)

    h, w, _ = p_img.shape
    m = cv2.getRotationMatrix2D((w / 2, h / 2), angle, 1)
    p_img = cv2.warpAffine(p_img, m, (w, h), borderValue=(127, 127, 127))

    if len(p_bounding_boxes) != 0:
        top_left = p_bounding_boxes[..., [0, 1]]
        top_right = p_bounding_boxes[..., [2, 1]]
        bottom_left = p_bounding_boxes[..., [0, 3]]
        bottom_right = p_bounding_boxes[..., [2, 3]]

        # N, 4, 2
        points = np.stack([top_left, top_right, bottom_left, bottom_right], axis=-2)
        points_3d = np.ones(points.shape[:-1] + (3,), np.float32)
        points_3d[..., :2] = points

        # points = m @ points_3d[0].T
        points = map(lambda x: m @ x.T, points_3d)
        points = np.array(list(points))
        points = np.transpose(points, [0, 2, 1])

        p_bounding_boxes[..., 0] = np.min(points[..., 0], axis=-1)
        p_bounding_boxes[..., 1] = np.min(points[..., 1], axis=-1)
        p_bounding_boxes[..., 2] = np.max(points[..., 0], axis=-1)
        p_bounding_boxes[..., 3] = np.max(points[..., 1], axis=-1)

        p_bounding_boxes[:, [0, 2]] = np.clip(p_bounding_boxes[:, [0, 2]], 0, w)
        p_bounding_boxes[:, [1, 3]] = np.clip(p_bounding_boxes[:, [1, 3]], 0, h)

    return p_img, p_bounding_boxes


def create_random_flip_lr(p_img, p_bounding_boxes):
    if np.random.randint(2):
        h, w, _ = p_img.shape
        p_img = p_img[:, ::-1, :]
        p_bounding_boxes[:, [0, 2]] = w - p_bounding_boxes[:, [2, 0]]

    return p_img, p_bounding_boxes


def create_random_crop_and_zoom(p_img, p_bounding_boxes, p_gts, size, jitter=0.3):
    net_w, net_h = size
    h, w, _ = p_img.shape
    dw = w * jitter
    dh = h * jitter

    rate = (w + np.random.uniform(-dw, dw)) / (h + np.random.uniform(-dh, dh))
    scale = np.random.uniform(1 / 1.5, 1.5)

    if (rate < 1):
        new_h = int(scale * net_h)
        new_w = int(new_h * rate)
    else:
        new_w = int(scale * net_w)
        new_h = int(new_w / rate)

    dx = int(np.random.uniform(0, net_w - new_w))
    dy = int(np.random.uniform(0, net_h - new_h))

    M = np.array([[new_w / w, 0., dx],
                  [0., new_h / h, dy]], dtype=np.float32)
    p_img = cv2.warpAffine(p_img, M, size, borderValue=(127, 127, 127))

    p_bounding_boxes[:, [0, 2]] = p_bounding_boxes[:, [0, 2]] * new_w / w + dx
    p_bounding_boxes[:, [1, 3]] = p_bounding_boxes[:, [1, 3]] * new_h / h + dy

    p_bounding_boxes[:, [0, 2]] = np.clip(p_bounding_boxes[:, [0, 2]], 0, net_w)
    p_bounding_boxes[:, [1, 3]] = np.clip(p_bounding_boxes[:, [1, 3]], 0, net_h)

    filter_b = np.logical_or(p_bounding_boxes[:, 0] >= p_bounding_boxes[:, 2], p_bounding_boxes[:, 1] >= p_bounding_boxes[:, 3])
    p_bounding_boxes = p_bounding_boxes[~filter_b]
    p_gts = p_gts[~filter_b]


    return p_img, p_bounding_boxes, p_gts


def filter_bboxes(p_img, p_bounding_boxes, p_gts):
    """
    Maginot Line
    """

    h, w, _ = p_img.shape

    x1 = np.maximum(p_bounding_boxes[..., 0], 0.)
    y1 = np.maximum(p_bounding_boxes[..., 1], 0.)
    x2 = np.minimum(p_bounding_boxes[..., 2], w - 1e-8)
    y2 = np.minimum(p_bounding_boxes[..., 3], h - 1e-8)

    int_w = np.maximum(x2 - x1, 0)
    int_h = np.maximum(y2 - y1, 0)
    int_area = int_w * int_h

    p_bounding_boxes = np.stack([x1, y1, x2, y2], axis=-1)
    # keep_idx = np.any(np.not_equal(p_bounding_boxes, 0), axis=-1)
    keep_idx = int_area > 0.
    return p_img, p_bounding_boxes[keep_idx], p_gts[keep_idx]


def augment_mosic(p_img, p_bounding_boxes, p_gts,
          p_img2, p_bounding_boxes2, p_gts2,
          p_img3, p_bounding_boxes3, p_gts3,
          p_img4, p_bounding_boxes4, p_gts4,
          min_offset=0.3):
    h, w = p_img.shape[0], p_img.shape[1]

    mix_img = np.zeros(shape=(h, w, 3), dtype='uint8')

    cut_x = np.random.randint(w * min_offset, w * (1 - min_offset))
    cut_y = np.random.randint(h * min_offset, h * (1 - min_offset))

    mix_img[:cut_y, :cut_x] = p_img[:cut_y, :cut_x]
    mix_img[:cut_y, cut_x:] = p_img2[:cut_y, cut_x:]
    mix_img[cut_y:, :cut_x] = p_img3[cut_y:, :cut_x]
    mix_img[cut_y:, cut_x:] = p_img4[cut_y:, cut_x:]

    keep_idx, p_bounding_boxes = clip_bbox_from_p_img(p_bounding_boxes, (0, 0, cut_x, cut_y))
    keep_idx2, p_bounding_boxes2 = clip_bbox_from_p_img(p_bounding_boxes2, (cut_x, 0, w, cut_y))
    keep_idx3, p_bounding_boxes3 = clip_bbox_from_p_img(p_bounding_boxes3, (0, cut_y, cut_x, h))
    keep_idx4, p_bounding_boxes4 = clip_bbox_from_p_img(p_bounding_boxes4, (cut_x, cut_y, w, h))

    mix_p_bounding_boxes = np.vstack((p_bounding_boxes, p_bounding_boxes2, p_bounding_boxes3, p_bounding_boxes4))
    mix_p_gts = np.vstack((p_gts[keep_idx], p_gts2[keep_idx2], p_gts3[keep_idx3], p_gts4[keep_idx4]))

    return mix_img, mix_p_bounding_boxes, mix_p_gts


def clip_bbox_from_p_img(p_bounding_boxes, target_bbox):
    tx1, ty1, tx2, ty2 = target_bbox

    x1 = np.maximum(p_bounding_boxes[..., 0], tx1)
    y1 = np.maximum(p_bounding_boxes[..., 1], ty1)
    x2 = np.minimum(p_bounding_boxes[..., 2], tx2)
    y2 = np.minimum(p_bounding_boxes[..., 3], ty2)

    new_bbox = np.stack([x1, y1, x2, y2], axis=-1)
    v_comput_ioa = comput_ioa(new_bbox, p_bounding_boxes)
    keep_idx = v_comput_ioa > 0.2

    return keep_idx, new_bbox[keep_idx]


def comput_ioa(p_bounding_boxes, target_p_bounding_boxes):
    w = np.maximum(p_bounding_boxes[..., 2] - p_bounding_boxes[..., 0], 0)
    h = np.maximum(p_bounding_boxes[..., 3] - p_bounding_boxes[..., 1], 0)

    tw = np.maximum(target_p_bounding_boxes[..., 2] - target_p_bounding_boxes[..., 0], 0)
    th = np.maximum(target_p_bounding_boxes[..., 3] - target_p_bounding_boxes[..., 1], 0)

    comput_ioa = w * h / np.maximum(tw * th, 1e-8)

    return comput_ioa




def image_mix_up(p_img, p_bounding_boxes, p_gts, p_img2, p_bounding_boxes2, p_gts2, alpha=None, beta=None):
    if alpha is None or beta is None:
        
        lambd = 0.5
    else:
        lambd = np.random.beta(beta, beta)

    H = max(p_img.shape[0], p_img2.shape[0])
    W = max(p_img.shape[1], p_img2.shape[1])
    mix_img = np.zeros(shape=(H, W, 3), dtype='float32')
    mix_img[:p_img.shape[0], :p_img.shape[1], :] = p_img.astype('float32') * lambd
    mix_img[:p_img2.shape[0], :p_img2.shape[1], :] += p_img2.astype('float32') * (1. - lambd)
    mix_img = mix_img.astype(np.uint8)

    mix_p_bounding_boxes = np.vstack((p_bounding_boxes, p_bounding_boxes2))
    mix_p_gts = np.vstack((p_gts, p_gts2))
    mix_weights = np.hstack((np.full(len(p_gts), lambd),
                             np.full(len(p_gts2), (1. - lambd))))

    return mix_img, mix_p_bounding_boxes, mix_p_gts, mix_weights


def create_onehot(p_gts, num_classes, smoothing):
    p_bounding_boxes_class = np.asarray(p_gts, dtype=np.int64)
    p_gts = np.eye(num_classes, dtype=np.float32)
    p_gts = p_gts[p_bounding_boxes_class]

    if smoothing:
        uniform_distribution = np.full(num_classes, 1.0 / num_classes)
        delta = 0.1
        p_gts = p_gts * (1 - delta) + uniform_distribution * delta

    return p_gts


